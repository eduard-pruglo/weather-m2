<?php
/**
 * Absolute Web Intellectual Property
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME, 
    'frontend/AbsoluteWeb/weather', 
    __DIR__
);