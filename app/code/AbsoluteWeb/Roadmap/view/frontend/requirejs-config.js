/**
 * Absolute Web Services Intellectual Property
 *
 * @category     {AWS/NewsletterSubscriber}
 * @copyright    Copyright © 1999-2018 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

'use strict';

// eslint-disable-next-line no-unused-vars
var config = {
    map: {
        '*': {
            'aws-get-weather': 'AbsoluteWeb_Roadmap/js/aws-get-weather',
            'aws-toggle-theme': 'AbsoluteWeb_Roadmap/js/aws-toggle-theme'
        }
    }
};