/**
 * Absolute Web Intellectual Property
 *
 * @category     {AbsoluteWeb/gloskinbeauty}
 * @copyright    Copyright © 1999-2021 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

'use strict';

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('aw.toggleChange', {
        options: {
            class: 'dark',
            button: '[data-role=data-label]'
        },

        /**
         * @private
         */
        _init: function() {
            this._bind();
        },

        /**
         * @private
         */
        _bind: function() {
            var _this = this;

            $(this.options.button).on('click', function() {
                $(document.body).toggleClass('dark');
            });
        }
    });

    return $.aw.toggleChange;
});

