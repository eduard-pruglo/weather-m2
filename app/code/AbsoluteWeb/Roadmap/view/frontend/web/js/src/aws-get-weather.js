/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2021 Absolute Web , Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

'use strict';

define([
    'ko',
    'jquery',
    'uiComponent',
], function(ko, $, Component) {

    return Component.extend({
        defaults: {
            template: 'AbsoluteWeb_Roadmap/weather',
            isLoading: false,
            isDayVisible: true,
            isWeekVisible: false,
            disabled: false,
            active: 'weather__link--active',
            todayWeatherArray: [],
            weekWeatherArray: [],
            location: ''
        },

        /**
         * Initializes class instance.
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();
            this.toggleTabs();
            this.getLocation();

            return this;
        },

        /**
         * Initializes observable properties.
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe('isLoading')
                .observe('isDayVisible')
                .observe('isWeekVisible')
                .observe('disabled')
                .observe('active')
                .observe('location')
                .observe('todayWeatherArray')
                .observe('weekWeatherArray');

            return this;
        },

        /**
         * Toggles content
         */
        toggleTabs: function () {
            let isDayVisible = !this.isDayVisible(),
                isWeekVisible = !this.isWeekVisible();

            this.isDayVisible(isDayVisible);
            this.isWeekVisible(isWeekVisible);
        },

        /**
         * @private
         * 
         * Receives data and updates it
         * 
         * @param {String} totalurl - getting full url
         */
        getData: function (totalurl) {
            let _this = this,
                url = totalurl;

            $.ajax({
                url: url,
                method: 'GET'
            }).done((response) => {
                _this.displayLocation(response.timezone);
                _this.weatherForDay(response.hourly);
                _this.weatherForWeek(response.daily);
            }).error(() => {
                console.log('Houston, we have a problem!');
            });
        },

        /**
         * @private
         * 
         * Makes a request to get the location of the device
         * 
         * @param {String} position - takes a GeolocationPosition object as its sole input parameter
         * @param {String} coordinates - gets an object with latitude and longitude
         */
        getLocation: function () {
            let _this = this,
                latitude,
                longitude,
                locationPromise;
            
            if (navigator.geolocation) {
                navigator.geolocation.watchPosition(function (position) {
                    return new Promise(function(resolve) {
                        setTimeout(function() {
                            latitude = position.coords.latitude;
                            longitude = position.coords.longitude;

                            locationPromise = {
                                lat: latitude,
                                long: longitude
                            };

                            resolve(locationPromise);
                        }, 1000);
                    }).then(function (coordinates) {
                        _this._getUrl(coordinates);
                    });
                }, _this.fail);
            }
        },
        
        /**
         * @private
         */
        fail: function () {
            console.log('Not today, sorry!');
        },

        /**
         * @private
         * 
         * @param {String} coordinates - gets an object with latitude and longitude
         */
        _getUrl: function (coordinates) {
            let baseUrl = 'https://api.openweathermap.org/data/2.5/onecall',
                apiKey = 'cab4d80c422dc1dbd7b02d6a1ab3a558',
                totalUrl = baseUrl + '?lat=' + coordinates.lat + '&lon=' + coordinates.long + '&appid=' + apiKey;

            this.getData(totalUrl);
        },

        /**
         * @private
         * 
         * @param {Object} place - user location to display on screen (continent / city)
         */
        displayLocation: function (place) {
            this.location(place);
        },

        /**
         * @private
         * 
         * @param {Object} today - array of objects with weather for every hour of the whole day
         * @returns {Object} Chainable.
         * 
         */
        weatherForDay: function (today) {
            let _this = this,
                hours = 24,
                eachHour = [];

            today.filter(function(value, index) {
                return index < hours;
            }).forEach(function(element) {
                return eachHour.push({
                    temp: Math.round(element.temp - 273.15) + '℃', 
                    dt: _this.ConvertUnixTime(element.dt), 
                    windSpeed: 'Wind speed: ' + element.wind_speed + ' m/s', 
                    description: element.weather[0].description,
                    icon: 'http://openweathermap.org/img/wn/' + element.weather[0].icon + '@2x.png'
                });
            });

            this.todayWeatherArray(eachHour);
        },

        /**
         * @private
         * 
         * @param {Object} week - array of objects with weather for every day of the whole week
         * @returns {Object} Chainable.
         * 
         */
        weatherForWeek: function (week) {
            let _this = this,
                daysInAWeek = 7,
                eachDay = [];

            console.log(week);

            week.filter(function(value, index) {
                return index < daysInAWeek;
            }).forEach(function(element) {
                return eachDay.push({
                    tempMax: 'max: ' + Math.round(element.temp.max - 273.15) + '℃', 
                    tempMin: 'min: ' + Math.round(element.temp.min - 273.15) + '℃', 
                    dt: _this.ConvertUnixTime(element.dt), 
                    windSpeed: 'Wind speed: ' + element.wind_speed + ' m/s', 
                    description: element.weather[0].description,
                    icon: 'http://openweathermap.org/img/wn/' + element.weather[0].icon + '@2x.png'
                });
            });

            this.weekWeatherArray(eachDay);
        },

        /**
         * @private
         * 
         * Epoch & Unix Timestamp Conversion Tools
         * 
         * @param {Object} date - array of objects with weather for every day of the whole week
         * @returns {Object} Chainable.
         * 
         */
        ConvertUnixTime: function (date) {
            let dateObject = new Date(date * 1000),
                humanDateFormat = dateObject.toLocaleString();

            return humanDateFormat;
        }
    });
});
